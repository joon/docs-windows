=================
 Troubleshooting
=================

.. highlight:: posh

General
=======

I suggest that you run SFC scan on the computer to see if fixes the issue. SFC
(System File Checker) checks for any corrupt system files and tries to repair
them. To do so, follow these steps.

#. At the Start screen, type ``cmd``.
#. You will find Command Prompt, right click on it and select ``Run as
   administrator`` from the bottom of the screen.  At the command prompt, type
   the following command, and then press enter: ``sfc /scannow``

The ``sfc /scannow`` command scans all protected system files and replaces
incorrect versions with correct Microsoft versions.

* Error log is located at::

   C:\$Windows.~BT\Sources\Panther\setuperr.log


Skydrive
========

Disable Skydrive in Windows 8.1
-------------------------------

http://www.ghacks.net/2013/11/02/disable-skydrive-windows-8-1/::

    HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Skydrive

You may need to create the Skydrive folder here. If you do, right-click on
Windows and select ``New`` > ``Key`` and name it ``Skydrive``.

Right-click on SkyDrive and select ``New > Dword (32-bit value)`` and name it
``DisableFileSync``.

Double-click the new parameter and change its value to ``1``.


Network
=======

* `Resolving proxy <https://support.google.com/chrome/answer/106010?hl=en>`_
* How to Delete or Forget Wireless Network Profiles in Windows 8.1::

   netsh wlan show profiles
   netsh wlan delete profile name="profile name"


0xd0000225 switch to live account
=================================

`This user Wasn't Added to this PC code
<http://answers.microsoft.com/en-us/windows/forum/windows_8-security/this-user-wasnt-added-to-this-pc-code-0xd0000225/cb8517ad-2d7e-43cb-9be6-daa760df159c?tm=1371326336045>`_

Add ``pku2u`` and ``livessp`` to the ``Security Packages`` ``REG_MULTI_SZ``
key under regkey ``HKLM\System\CurrentControlSet\Control\Lsa``, and reboot.


Windows 8 sleeps after few minutes when awaken by external USB keyboard

=======================================================================

`Forum
<http://social.technet.microsoft.com/Forums/en-US/w8itprogeneral/thread/7218c4d2-5829-4e99-af27-8695657fab4d/>`_

#. Get registry from `Here
   <http://www.sevenforums.com/tutorials/246364-power-options-add-system-unattended-sleep-timeout.html>`_
   and add "System unattended sleep timeout" to Advanced Power Settings

#. Following `Here
   <http://www.sevenforums.com/tutorials/35767-sleep-return-timeout-unattended-wake-up.html>`_,
   increase system unattended sleep timeout.
