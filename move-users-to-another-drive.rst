Move ``C:\Users`` directory to ``D:\Users``
=============================================

.. warning:: This is not recommended anymore:
             http://support.microsoft.com/kb/949977/en-us

When I use Windows, I always have separate partition for the data (``D:``). I
have never tried to actually move the whole ``Users`` folder to ``D:`` though.
Instead I moved folders with data such as Documents to ``D:`` manually after
installing Windows.

After purchasing a couple of SSD's for my laptops, I decided to install Windows 8
on them, and decided to use more sophisticated (and elegant) method of moving 
Users folder to ``D:``, which leverages the soft link functionality of Windows.

I have read some howto's about this and at first they look complicated, but it
turns out it is pretty simple (about 3 lines of commands) and robust. As
usual, it was so much better than manual way.

There are just a couple of things which can mess things up if you are not
careful, and I wanted to share the experience so others can do this without
trial and error.

I assume you already have separate partition ``D:``.

Dropping to the Command Line Prompt
-----------------------------------

Windows 8
~~~~~~~~~

Since no process should be using files in the ``C:\Users`` directory, you
have to drop to the recovery mode command line prompt.  There are two ways
that I know of. You can do it by booting with installation DVD and also you
can do it in installed Windows 8. I recommend the second method as it is much
faster. It became very easy in Windows 8 since you can invoke this within
running Windows.

Please see the following link for how-to: `How to start the Recovery
Environment Command Prompt in Windows 8
<http://www.bleepingcomputer.com/tutorials/windows-8-recovery-environment-command-prompt/#start>`_

Windows 7
~~~~~~~~~

Please see the following website for how-to: `How to Get to the Recovery
Console in Windows
<http://www.dummies.com/how-to/content/how-to-get-to-the-recovery-console-in-windows.navId-323028.html>`_

Figure out drive letters
------------------------

This is the first (a little bit) tricky part. For some reason when you drop to
this command prompt the drive letters are different. In my case, it was always
``X:`` which was the secovery partition, and ``C:`` became ``D:`` and ``D:``
became ``E:``.  So when you are working in ``D:`` in the prompt, actually you
are at ``C:``.  This can be confusing but once you figure it out it is not
that bad. Anyway, if your drive letter mapping is different from mine, you
have to adjust the following commands accordingly. Basically, when you make
copies you use the letters assigned in the command prompt, but when you make
the link, you have to use the letters that windows uses.


Copy ``Users`` folder to ``D:\Users``
-------------------------------------

Two things are important in this step: 1) you have to use the correct drive
letters, and 2) you have to use the exact options for ``robocopy``. If you mess up
1), you won't be able to log in, and if you mess up 2), then you might be able
to log in but will have other problems.

Do the following to copy ``C:\Users`` to ``D:\Users``. Again, keep in mind the
drive letters are remapped::

   X:> D:
   D:> robocopy /copyall /mir /xj Users E:\Users

``/mir`` option will delete all files in the target if they do not exist in
the source. Hence, if you already have ``D:\Users`` and want to keep the
files, then use ``/E`` option::

   D:> robocopy /copyall /E /xj Users E:\Users

Again, here ``D:`` is ``C:`` in actual, and ``E:`` is ``D:`` in actual.  Among
robocopy options, if you miss ``/copyall``, then it will not copy the
permission information, and after log-in your Windwos 8 apps will crash upon
launching.  Make sure you get 0 for failed column in the robocopy results
output.

After verifying there were no error, do the following to remove the original
``C:\Users`` directory::

   D:> rmdir /S /Q Users


Create the symbolic link
------------------------

This is the final step, creating the symbolic link at ``C:\Users`` which
points to ``D:\Users``. Here the drive letter thing again comes up. Since the
symbolic link will be used in actual windows session, you have to use the
actual drive letter ,``D:`` as the target. So you end up making a symbolic
link at ``D:\Users`` (command prompt letter - ``C:`` in actual) whose target
is also ``D:\Users``. Since you are already at ``D:``, use the following so
it is less confusing::

   D:> mklink /J Users D:\Users
   Junction created for Users <<===>> D:\Users

I have not tried other options, but it seems if you don't use ``/J``
(Junction) option you will have to do a registry hack.

You can verify the link with ``dir`` command. You will see::

   <JUNCTION>      Users [D:\Users]

Anyway, that's it. When you exit and reboot (select continue to windows 8),
you will be able to log in, and when you open up the explorer, you will see
that ``C:\Users`` is a symbolic link which points to ``D:\Users``.::

   D:> exit

Again, the steps are very easy when you figure it out (especially the drive 
letter thing) but at the same time it is very easy to make a mistake. 



Using BitLocker
---------------

You can also use BitLocker.

1. Encrypt ``C:`` with BitLocker
2. Encrypt ``D:`` with BitLocker, and make sure to enable Auto-Unlock

Then when boot, Windows will unlock ``D:`` so you can log in.
