==============
 Installation
==============

.. highlight:: posh

.. begin-common

.. end-common

SysAdmin
========

* `Chocolatey <http://chocolatey.org/>`_::

    @powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%systemdrive%\chocolatey\bin

.. code:: posh

   choco install Tunnelier 

.. begin-cron

* `Windows Cron Service
  <http://www.intelliadmin.com/index.php/2012/05/free-utility-a-simple-task-scheduler-for-windows/>`_

.. end-cron

* `Paragon ExtFS for Windows
  <http://www.paragon-software.com/home/extfs-windows/>`_ : ext4 Read/Write support
* `CrashPlan <http://www.code42.com/crashplan/>`_

  * See `How to Move Your CrashPlan Backups to a New Computer
    <http://lifehacker.com/5893647/how-to-move-your-crashplan-backups-to-a-new-computer>`_
    to restore your backup settings

* `Tiny Windows Borders <http://winaero.com/download.php?view.31>`_

.. code:: posh

   choco install LinkShellExtension f.lux pscx wifi-manager

* `BgInfo <http://technet.microsoft.com/en-us/sysinternals/bb897557.aspx>`_

Browsers
========

.. code-block:: posh

   choco install Firefox GoogleChrome  # browsers

Cloud
=====

.. code-block:: posh

   choco install dropbox googledrive

Devel
=====

.. code-block:: posh

   choco install Emacs geany nano KickAssVim SublimeText3  # Editors; KickAssVim will pull ctags and vim
   choco install consolez git.install meld winmerge  # devel
   choco install SourceCodePro  # fonts; this doesn't work it seems

Pinning Emacs to the taskbar
----------------------------

http://superuser.com/questions/259146/why-latest-emacs-version-dont-support-windows-7-taskbar

If you just pin Emacs icon on the taskbar, next time you click it to launch
Emacs, it will have a terminal window as well as GUI window. In order to fix
this,

#. Run ``runemacs.exe`` with no pre-existing icon on the taskbar.
#. Right click on the running Emacs icon in the taskbar, and click on "pin
   this program to taskbar" item.
#. Close the Emacs
#. Shift right-click on the pinned Emacs icon on the taskbar, click on
   Properties, and change the target from ``emacs.exe`` to ``runemacs.exe``.

Office
======

.. code-block:: posh

   choco install PDFXchangeEditor PDFCreator  # PDF

* It is better to install `Sumatra PDF
  <http://blog.kowalczyk.info/software/sumatrapdf/download-free-pdf-viewer.html>`_
  from the installer so you can install the browser plugins and pdf preview.

Multimedia
==========

.. code-block:: posh

   choco install foobar2000 mpc-hc

Communications
==============

.. code-block:: posh

   choco install winscp deluge youtube-dl

Utils
=====

.. code-block:: posh

   choco install 7zip autohotkey_l dexpot teamviewer  # utils


Chocolatey
==========

.. code-block:: posh

   choco install IcoFx
   choco install SourcePreviewHandler 
   choco install keepass  # pims
   choco install paint.net gimp 

   choco install pandoc


Libraries
=========

GTK
---

In both cases, get the all-in-one bundle.

* `for Windows (32-bit) <http://www.gtk.org/download/win32.php>`_ (Get the 2.x
  version)
* `for Windows (64-bit) <http://www.gtk.org/download/win64.php>`_

Set ``PATH``::

   setx -m PATH "%PATH%;C:\lib\gtk\bin"

Utilities
=========

unison
------


Aspell
------

#. Download Aspell executable (``Full installer``) and at least one dictionary
   (``aspell-en-0.50-2-3.exe``) from http://aspell.net/win32/.
#. Install ``Aspell-0-50-3-3-Setup`` and then ``Aspell-en-0.50-2-3``.


awk
---

.. begin-awk

* Download and install `Gawk for Windows
  <http://gnuwin32.sourceforge.net/packages/gawk.htm>`_
* Put ``C:\Program Files (x86)\GnuWin32\bin\`` into your ``Path``::

      setx -m PATH "%PATH%;"C:\Program Files (x86)\GnuWin32\bin"

.. end-awk

cURL
----

.. begin-cURL

* If you have `Chocolatey <http://chocolatey.org/>`_ installed (which I highly recommend)::

   $ choco install curl

* Otherwise, follow instructions `here
  <http://waheedtechblog.blogspot.com/2011/10/downloading-and-installing-curl-on.html>`_.

* Also, follow the instructions `here
  <http://waheedtechblog.blogspot.com/2011/10/downloading-and-installing-curl-on.html>`_
  to add ``Certficate Authority Public Keys`` for ``https``.

.. end-cURL

ClassicShell
------------

* `Get the best looking Start Menu for Classic Shell 4+ with Winaero Skin 2.0
  <http://winaero.com/blog/get-the-best-looking-start-menu-for-classic-shell-4-with-winaero-skin-2-0/>`_
* `windows 8.1 ms clone button
  <http://classicshell.net/forum/viewtopic.php?f=18&t=1012>`_

Others
------

* `Clipboard Help+Spell
  <http://www.donationcoder.com/Software/Mouser/clipboardhelpandspell/index.html>`_
* `Free Alarm Clock <http://freealarmclocksoftware.com/>`_
* `DiskInternals Linux Reader <http://www.diskinternals.com/linux-reader/>`_
* `SQLiteSpy <http://www.yunqa.de/delphi/doku.php/products/sqlitespy/index>`_


Multimedia
==========

foobar2000
----------

* `Columns UI <http://yuo.be/columns.php>`_
* `Quick Tagger <http://www.foobar2000.org/components/view/foo_quicktag>`_
* `flac <https://xiph.org/flac/download.html>`_

* `mplayer2 <https://code.google.com/p/mplayer2-for-windows/>`_


Preview Handlers
================

PDF Preview
-----------

For PDF Preview in Explorer and Directory Opus, get `PDF XChange Viewer
<http://www.tracker-software.com/product/pdf-xchange-viewer/download>`_ and
install Shell Extensions. It is by default checked when you install it.
Portable version does not have this plugin.

See http://www.tracker-software.com/shell_ext.html for explanations.

Source Code
-----------

* `Source Preview Handler <http://www.smartftp.com/client/addons/sourcepreview>`_

Fonts
=====

* `나눔고딕 코딩 <http://dev.naver.com/projects/nanumfont/download>`_

