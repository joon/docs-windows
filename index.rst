.. Documentation: Windows documentation master file, created by
   sphinx-quickstart on Tue Apr 16 13:10:02 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Documentation: Windows's documentation!
==================================================

Contents:

.. toctree::
   :maxdepth: 2

   move-users-to-another-drive
   admin
   installation
   powershell
   python
   anaconda
   devel
   latex
   backup
   troubleshooting

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

