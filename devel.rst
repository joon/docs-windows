===================
 Devel Environment
===================

General
=======

Git
===

* `Git for Windows <http://msysgit.github.io/>`_

* Install it with default settings (I tend to use option 3 so unix commands
  are available in other shell)

* Git Bash command line::

   C:\Windows\SysWOW64\cmd.exe /c ""C:\Program Files (x86)\Git\bin\sh.exe" --login -i"

* Just use Chocolatey to install it::

  choco install git  

* When you try to update it with Chocolatey, you might get an error saying
  ``Setup cannot continue until you close at least those applications in the
  list that are marked as "closing is required".`` Stop ``ssh.exe`` and try it
  again::

     Get-Process ssh.exe | Stop-Process

PowerShell
==========

posh-git
--------

https://github.com/dahlbyk/posh-git

A set of PowerShell scripts which provide Git/PowerShell integration


Terminal
========

* PowerShell
* ConsoleZ
* ConEmu

Libraries
=========

GSL
---

Download
~~~~~~~~

You can download binary GSL from `oscats 
<https://code.google.com/p/oscats/downloads/list>`_ projects. Make sure you
get the correct architecture. (Usually 64bit)


Environmental Variables
~~~~~~~~~~~~~~~~~~~~~~~

Add the path to \bin directory of your GSL installation to your PATH
Environmental variable::

   setx -m PATH "$env:path;C:\lib64\gsl\bin"

Add an environmental variable LIB_GSL with the path to your GSL installation::

   setx -m LIB_GSL "C:/lib64/gsl"

.. note:: You need to run above commands in administrator mode in CMD.


Compilers
=========

Windows SDK C/C++
-----------------

https://github.com/cython/cython/wiki/64BitCythonExtensionsOnWindows
http://rexdouglass.com/python-64-bit-on-windows-part-2-building-packages


MinGW with OpenMP support
-------------------------

MinGW doesn't come with openmp by default so if it's not included in the
Anaconda version you'll probably have to find the correct installer from the
MinGW site and install it.

https://groups.google.com/a/continuum.io/forum/#!topic/anaconda/c-4u6C29hYM

.. warning:: The cython `wiki
             <https://github.com/cython/cython/wiki/64BitCythonExtensionsOnWindows>`_
             Does not recommend using MinGW for the 64bit compiler.

Install TDM-GCC
~~~~~~~~~~~~~~~

`TDM-GCC <http://tdm-gcc.tdragon.net/>`_ is a compiler suite for Windows.

    It combines the most recent stable release of the GCC toolset with the
    free and open-source MinGW or MinGW-w64 runtime APIs to create a LIBRE
    alternative to Microsoft's compiler and platform SDK

``openmp`` support must be manually added during installation. Expand
``Components - gcc`` and check ``openmp``:

.. figure:: images/TDM-GCC-setup.PNG
   :scale: 100 %
   :alt: TDM-GCC Installation

   TDM-GCC Installation

