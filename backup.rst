========
 Backup
========

LyX settings
============

In ```C:\Users\User\AppData\Roaming\LyX2.X`` folder:

.. code-block:: sh

   preferences  # general preferences
   pwl_english.dict  # personal word list
   bind\user.bind  # key binding

Outlook
=======

``*.ost`` files in In ```C:\Users\User\AppData\Local\Microsoft\Outlook`` folder.


Windows 8 Apps
==============

Manga Z

.. code-block:: sh

   C:\Users\joon\AppData\Local\Packages\36032Twincubesstudio.MangaZ_3znhphxp8kcdy
