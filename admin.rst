=======
 Admin
=======

.. highlight:: sh

Environmental Variables
=======================

* System environmental variable::

   setx -m PATH "%PATH%;.\"
   setx -m PATH "%PATH%;C:\Program Files (x86)\KDE\bin"

* Local environmental variables::

   setx HOME "C:\Users\Joon"
   setx PATH "%PATH%;%HOME%\bin"


Symbolic Links
==============

.. code-block:: sh

  > mklink /J c:\home %home%..\..
  > mklink /J %home%\.lyx %appdata%\LyX2.0
  > mklink /J %home%\.mozilla\firefox "%appdata%\Mozilla\Firefox"
  > mklink /J %home%\.config\sublime-text-3 "%appdata%\Sublime Text 3"
  > mklink /J %home%\vimfiles "%home%\.vim"
  > mklink /J %home%\.kde4 "%appdata%\.kde"
  > mklink /J %home%\.wingide5 "%appdata%\Wing IDE 5"
  > mklink /J C:\lib D:\lib
  > mklink /J C:\lib64 D:\lib64

  > mklink /J Documents D:\Users\joon\Documents
  > mklink /J Downloads D:\Users\joon\Downloads
  > mklink /J Music D:\Users\joon\Music
  > mklink /J Pictures D:\Users\joon\Pictures
  > mklink /J Videos D:\Users\joon\Videos

  > mklink /J Copy D:\Users\joon\Copy
  > mklink /J Comics D:\Users\joon\Comics
  > mklink /J Data D:\Users\joon\Data
  > mklink /J AeroFS D:\Users\joon\AeroFS
  > mklink /J Dropbox D:\Users\joon\Dropbox
  > mklink /J ".\Google Drive" "D:\Users\joon\Google Drive"


PowerShell Remoting
===================

To configure the computer (the server) to receive Windows PowerShell remote
commands, run the following in an elevated PowerShell::

   Enable-PSRemoting


SSH
===

SSH Server
----------

* Download and install `Bitvise SSH Server <http://www.bitvise.com/winsshd>`_::

     choco install winsshd 

SSH Client
----------

* Download `Bitvise SSH Client (Tunnelier)
  <http://www.bitvise.com/ssh-client-download>`_::

     choco install Tunnelier

* You can use ``ssh`` comes with Git Bash as well.

Bitvise SSH Server Public key authentication
--------------------------------------------

* Generate public/private rsa key pair::

   ssh-keygen

* To be able to add public keys to the server from the client, download
  `Bitvise SSH Server Remote Control Panel
  <http://www.bitvise.com/wrc-download>`_.
* Then follow instructions here: http://www.bitvise.com/wug-publickey

#. Login to the server
#. Open Bitvise SSH Server Control Panel
#. ``Open Easy Settings``
#. ``2. Windows accounts``
#. From the ``Public keys imported`` tab, you can import local public
   keys. (Import both bitvise-generated and ssh-keygen generated keys)


Tinkerer blog
=============

.. code-block:: bat

   $ cd ~/git
   $ git clone git@github.com:joonro/blog.git
   $ mklink /J en C:\Users\joon\Dropbox\tinkerer\en\blog\html
   $ mklink /J ko C:\Users\joon\Dropbox\tinkerer\ko\blog\html


공인인증서
==========

The location of 공인인증서 in Windows is::

   %APPDATA%\..\LocalLow\NPKI

