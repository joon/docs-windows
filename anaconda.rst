=======================
 Anaconda Installation
=======================

Download
========

Anaconda download page:
https://store.continuum.io/cshop/anaconda

Path setup
----------

.. code-block:: sh

   $ export anaconda=C:\Anaconda
   $ PYTHON=$anaconda

pylauncher
----------

`pylauncher <https://bitbucket.org/vinay.sajip/pylauncher/downloads>`_ for
command line python script launching

Mayavi
======

.. deprecated:: 1.7.0
   Use ``conda install mayavi`` instead.

http://www.lfd.uci.edu/~gohlke/pythonlibs/ maintains fantastic repository of
binary Python libraries. To install Mayavi, you need to install the following
two packages::

   VTK-5.10.1.win-amd64-py2.7
   ets-4.3.0.win-amd64-py2.7

* ets `ets <http://www.lfd.uci.edu/~gohlke/pythonlibs/#ets>`_
* `VTK <http://www.lfd.uci.edu/~gohlke/pythonlibs/#vtk>`_

For some reason, if you install ``VTK-Qt-5.10.1.win-amd64-py2.7``, it yields DLL
error.

And also, install ``configobj`` if you get import failure with ``configobj``::

   pip install configobj

ViTables
========

From http://www.lfd.uci.edu/~gohlke/pythonlibs/, download the following
packages, `PyQt <http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyqt>`_ and
`ViTables <http://www.lfd.uci.edu/~gohlke/pythonlibs/#vitables>`_::

   PyQt-Py2.7-x64-gpl-4.9.6-1.‌exe
   ViTables-2.1.win-amd64-py2.7.‌exe

You can run ViTables with the following commands (use it as shortcut target)::

   C:\Anaconda\pythonw.exe "C:\Anaconda\Scripts\vitables"

Update Packages
===============

Using conda
-----------

Accelerate
~~~~~~~~~~

http://docs.continuum.io/accelerate/

.. code-block:: sh

   $ conda update conda
   $ conda install accelerate


Others
~~~~~~

.. note::
   
   Make sure you run ``conda`` with all python app closed. Otherwise, it might
   fail with permission error.

.. code-block:: sh

   $ conda remove pip
   $ conda update distribute cython matplotlib pandas scipy
   $ conda install pip


Using pip
---------

.. code-block:: sh

   $ pip install bottleneck virtualenvwrapper-powershell


Manual
------

CythonGSL
~~~~~~~~~

.. code-block:: sh

   $ git clone git@github.com:joonro/CythonGSL.git
   $ cd CythonGSL
   $ git remote add upstream git://github.com/twiecki/CythonGSL.git
   $ python setup.py install

IPython
~~~~~~~

.. code-block:: sh

   $ rm -rf $anaconda/Lib/site-packages/IPython $anaconda/Lib/site-packages/ipython*
   $ python setup.py install

Statsmodels
~~~~~~~~~~~

.. code-block:: sh

   $ rm -rf $anaconda/Lib/site-packages/statsmodels*
   $ pip install patsy

   $ git clone git@github.com:joonro/statsmodels.git
   $ cd Statsmodels
   $ python setup.py install

Cython
~~~~~~

.. code-block:: sh

   $ rm -rf $anaconda/Lib/site-packages/Cython* $anaconda/Lib/site-packages/cython.*
   $ python setup.py install


Virtualenv
----------

virtualenvwrapper-powershell is installed as a module in
``~/Documents/WindowsPowerShell/Modules``. To load the module, do ``import-module
virtualenvwrapper.``

Also, it will complain about not finding Python ``InstallPath`` from the
registry. Copy ``InstallPath`` and ``PythonPath`` from
``HKLM\Software\Python\PythonCore\2.7`` to ``HKCU\Software\Python\PythonCore\2.7``.
