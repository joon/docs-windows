===========
 LaTeX/LyX
===========

.. highlight:: bat

Global location of ``.bib`` file.
=================================
http://tex.stackexchange.com/questions/49126/common-bib-file-for-several-collaborators

As in GNU/Linux, you can make ``texmf\bibtex\bib`` directory under the user's
profile directory (``C:\Users\username``).

.. code-block:: sh

   mkdir ~/texmf
   mkdir ~/texmf/bibtex
   mkdir ~/texmf/bibtex/bib

The following is copied from the stackexchange entry:

#. On Windows with TeX Live, make a ``texmf\bibtex\bib`` directory under the
   user's profile directory (``C:\Users\username`` by default on Windows Vista and
   higher).
#. Place the .bib file in that folder.
#. Place any custom bibliography styles in a ``texmf\bibtex\bst`` directory under
   the user's profile directory.
#. Pull up a command prompt, and run ``mktexlsr texmf`` from the profile directory
   (I don't remember if TeX Live automatically has this in the path or not, so
   you may need to run it as ``C:\texlive\2011\bin\win32\mktexlsr texmf`` or
   similar instead).
#. Write your documents wherever, and use the regular \bibliography and
   related commands as usual. No need for paths, since your personal texmf
   tree will automatically be searched for support files.


Inverse search with LyX
=======================

http://wiki.lyx.org/LyX/SyncTeX#toc3

#. Set ``Tools`` > ``Preferences`` > ``Paths`` > ``LyxServer pipe`` to
   ``\\.\pipe\lyxpipe``.
#. In ``Document`` > ``Settings`` > ``Output``, check ``Synchronize with output``.
#. Create a batch-file named ``lyxeditor.cmd`` and save it to one of the
   locations in your ``PATH`` Windows environmental variable::
  
      @echo off
      SETLOCAL enabledelayedexpansion
      set file=%1
      set row=%2
      REM remove quotes from variables 
      set file=!file:"=!
      set row=!row:"=!
      %comspec% /q /c (@echo LYXCMD:revdvi:server-goto-file-row:%file% %row%)> \\.\pipe\lyxpipe.in&&type \\.\pipe\lyxpipe.out
      endlocal   


AutoHotkey Script
-----------------

The above cmd works well, but it shows annoying black cmd window everytime
when you invoke the script. With a simple AutoHotkey script, one can not only
suppress this window, but also activate LyX windows after the inverse search.

Create a AutoHotkey script named ``lyx-inverse-search.ahk`` with the following
code and save it to the same location to ``lyxeditor.cmd`` and compile it with
AutoHotkey to generate ``lyx-inverse-search.exe``:

.. code-block:: ahk

   SetTitleMatchMode, RegEx
   Run, lyxeditor.cmd "%1%" "%2%",, Hide
   WinActivate, ^LyX:,,,

If you don't have AutoHotkey installed, you can download the compiled `exe
<https://dl.dropboxusercontent.com/u/561594/lyx-inverse-search.zip>`_.

SumatraPDF
----------

http://wiki.lyx.org/Windows/LyXWinTips#toc6

#. Download and install `Sumatra PDF
   <http://blog.kowalczyk.info/software/sumatrapdf/download-free-pdf-viewer.html>`_.
#. In LyX go to Tools > Preferences > Paths and add the install location to ``PATH``
   prefix. Most likely this is ``C:\Program Files\SumatraPDF``.

   .. note:: If you use Chocolatey to install Sumatr PDF, the PATH is ``C:\Chocolatey\bin``

#. In ``Tools`` > ``Preferences`` > ``File Handling`` > ``File Formats``
   select ``PDF (pdflatex)`` from the list of formats and modify ``Viewer`` to
   the following::

      SumatraPDF -reuse-instance -inverse-search "lyx-inverse-search.exe \"%f\" \"%l\""

#. If you don't have the compiled AutoHotkey script from above, use the following::

      SumatraPDF -reuse-instance -inverse-search "lyxeditor.cmd \"%f\" \"%l\""


Okular
------

#. In LyX go to ``Tools`` > ``Preferences`` > ``Paths`` and add the location
   of ``okular.exe`` to ``PATH`` prefix. Most likely this is ``C:\Program
   Files (x86)\KDE\bin``
#. In Tools > Preferences > File Handling > File Formats select PDF (pdflatex)
   from the list of formats and modify Viewer to::

      okular --unique

#. In Okular, ``Settings`` > ``Configure Okular`` > ``Editor``, choose
   ``Custom Text Editor`` and input the following::

       lyx-inverse-search.exe "%f" "%l"

   If you don't have the compiled AutoHotkey script from above, use the following::

       lyxeditor.cmd "%f" "%l"

.. note:: If ``lyxeditor.cmd`` or ``lyx-inverse-search.exe`` is not in your system
          ``PATH``, then you have to specify the full path here.

Forward search with LyX
=======================

SumatraPDF
----------

Forward search setting is easier. Make sure you have the `PATH` to Sumatra PDF
in Tools > Preferences > Paths. Then in Tools > Preferences > Output >
General, choose ``SumatraPDF -reuse-instance $$o -forward-search $$t $$n``,
which should be built-in.

.. note:: If you have master and child document structure, the forward search
          only works when you compile the full document. If you only compile
          current child document forward search will not work.

Okular
------

.. code-block:: bat

   okular --unique "file:$$o#src:$$n $$f"

Mendeley Settings
=================

``Tools`` -> ``Options`` -> ``BibTex``, check ``Enable BibTeX
syncing``. ``Create one BibTeX file per collection``. Path, put
``C:\Users\joon\texmf\bibtex\bib``. 

