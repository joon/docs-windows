Python Environment
==================

* `The Scipy Stack specification <http://scipy.github.io/stackspec.html>`_

Resources
---------

* `Enthought Canopy <https://www.enthought.com/products/canopy/>`_
* `Anaconda <https://store.continuum.io/cshop/anaconda>`_
* `Python(x,y) <https://code.google.com/p/pythonxy/>`_
* `Unofficial Windows Binaries for Python Extension Packages
  <http://www.lfd.uci.edu/~gohlke/pythonlibs/>`_ 

   * For individual packages.
   * Also, it has both 32 and 64-bit packages.

All of the distributions come with MinGW.

If you need ``openmp`` support in Cython, then you should install `TDM-GCC
<http://tdm-gcc.tdragon.net/>`_ 
